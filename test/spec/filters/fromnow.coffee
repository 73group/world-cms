'use strict'

describe 'Filter: fromNow', ->

  # load the filter's module
  beforeEach module 'worldCmsApp'

  # initialize a new instance of the filter before each test
  fromNow = {}
  beforeEach inject ($filter) ->
    fromNow = $filter 'fromNow'

  it 'should return the input prefixed with "fromNow filter:"', ->
    text = new Date
    expect(fromNow text).toBe 'несколько секунд назад'
