'use strict'

describe 'Controller: UsersController', ->

  # load the controller's module
  beforeEach module 'worldCmsApp'

  UsersController = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    UsersController = $controller 'UsersController', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(3).toBe 3
