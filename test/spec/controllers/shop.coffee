'use strict'

describe 'Controller: ShopController', ->

  # load the controller's module
  beforeEach module 'worldCmsApp'

  ShopController = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    ShopController = $controller 'ShopController', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(3).toBe 3
