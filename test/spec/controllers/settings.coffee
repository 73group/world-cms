'use strict'

describe 'Controller: SettingsController', ->

  # load the controller's module
  beforeEach module 'worldCmsApp'

  SettingsController = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    SettingsController = $controller 'SettingsController', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(3).toBe 3
