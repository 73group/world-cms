'use strict'

describe 'Controller: ApplicationController', ->

  # load the controller's module
  beforeEach module 'worldCmsApp'

  ApplicationController = null
  rootScope = null
  scope = null
  location = null

  userMock =
    id:1
    role_id:1
    city_id:null
    remember_token:''
    username:'root'
    email:'root@gmail.local'
    first_name:'Root'
    last_name:''
    surname:'Admin'
    phone:'+7800200060'
    birthday:'1990-01-01'
    created_at:'2014-06-13 19:50:06'
    updated_at:'2014-06-13 19:50:06'
    deleted_at:null
    access_token:'$2y$10$MH6bcwTNFbHY0Qb.fI812OqkGVXdARVducgfhnxw93meC9haZNN'

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope, $location, $httpBackend, apiBase) ->
    controller = $controller
    rootScope = $rootScope
    scope = $rootScope.$new()
    ApplicationController = $controller 'ApplicationController', {
      $scope: scope
    }
    $httpBackend.expectGET apiBase + 'users/me'
      .respond 401
    location = $location
    httpBackend = $httpBackend
    apiRoot = apiRoot

  it 'should be show partials', ->
    expect(rootScope.showPartials).toBe yes

  it 'should protect routes for unautorized users', ->
    location.path('/profile')
    rootScope.$apply()
    expect(location.path()).toBe '/login'

  it 'should allow routes for autorized users', ->
    window.localStorage.setItem 'access_token', 'some token'
    location.path('/profile')
    rootScope.$apply()
    expect(location.path()).toBe '/profile'

  it 'should be logged out', ->
    scope.logout()
    location.path('/profile')
    rootScope.$apply()
    expect(location.path()).toBe '/login'
    token = window.localStorage.getItem 'access_token'
    expect(token).toBe null