'use strict'

describe 'Controller: SiteController', ->

  # load the controller's module
  beforeEach module 'worldCmsApp'

  SiteController = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    SiteController = $controller 'SiteController', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(3).toBe 3
