'use strict'

describe 'Controller: CityController', ->

  # load the controller's module
  beforeEach module 'worldCmsApp'

  CityController = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    CityController = $controller 'CityController', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(3).toBe 3
