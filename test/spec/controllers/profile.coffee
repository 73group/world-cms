'use strict'

describe 'Controller: ProfileController', ->

  # load the controller's module
  beforeEach module 'worldCmsApp'

  ProfileController = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    ProfileController = $controller 'ProfileController', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(3).toBe 3
