'use strict'

describe 'Controller: RegionController', ->

  # load the controller's module
  beforeEach module 'worldCmsApp'

  RegionController = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    RegionController = $controller 'RegionController', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(3).toBe 3
