'use strict'

describe 'Controller: LoginController', ->

  # load the controller's module
  beforeEach module 'worldCmsApp'

  LoginController = {}
  scope = {}

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    LoginController = $controller 'LoginController', {
      $scope: scope
    }

  it 'should attach a list of awesomeThings to the scope', ->
    expect(3).toBe 3
