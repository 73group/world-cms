'use strict'

describe 'Service: Role', ->

  # load the service's module
  beforeEach module 'worldCmsApp'

  # instantiate service
  Role = {}
  beforeEach inject (_Role_) ->
    Role = _Role_

  it 'should do something', ->
    expect(!!Role).toBe true
