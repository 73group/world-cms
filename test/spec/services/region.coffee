'use strict'

describe 'Service: Region', ->

  # load the service's module
  beforeEach module 'worldCmsApp'

  # instantiate service
  Region = {}
  beforeEach inject (_Region_) ->
    Region = _Region_

  it 'should do something', ->
    expect(!!Region).toBe true
