'use strict'

describe 'Service: Site', ->

  # load the service's module
  beforeEach module 'worldCmsApp'

  # instantiate service
  Site = {}
  beforeEach inject (_Site_) ->
    Site = _Site_

  it 'should do something', ->
    expect(!!Site).toBe true
