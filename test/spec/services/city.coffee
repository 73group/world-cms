'use strict'

describe 'Service: City', ->

  # load the service's module
  beforeEach module 'worldCmsApp'

  # instantiate service
  City = {}
  beforeEach inject (_City_) ->
    City = _City_

  it 'should do something', ->
    expect(!!City).toBe true
