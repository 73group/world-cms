'use strict'

describe 'Directive: serverError', ->

  # load the directive's module
  beforeEach module 'worldCmsApp'

  scope = {}

  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()

  it 'should make hidden element visible', inject ($compile) ->
    element = angular.element '<server-error></server-error>'
    element = $compile(element) scope
    expect('').toBe ''
