'use strict'

angular.module('worldCmsApp')
  .controller 'CitiesController', ($scope, $filter, Region, City) ->
    $scope.filter =
      region: null

    $scope.regions = Region.all()  
    $scope.cities  = City.all()

    $scope.remove = (city) ->
      $scope.notifications.push
        message: 'Происходит удаление города'
        status: 'info'
      city.$remove id:city.id
      , (success) ->
        city.deleted_at = $filter('date')(new Date(),'yyyy-MM-dd HH:mm:ss')
        $scope.notifications.push
          message: 'Город успешно удален'
          status: 'success'
      , (errors) ->
        $scope.notifications.push
          message: errors.data
          status: 'danger'

    $scope.restore = (city) ->
      $scope.notifications.push
        message: 'Происходит восстановление города'
        status: 'info'
      deletedAt = city.deleted_at
      city.deleted_at = null
      city.$restore id:city.id
      , (success) ->
        $scope.notifications.push
          message: 'Город успешно восстановлен'
          status: 'success'
      , (error) ->
        city.deleted_at = deletedAt
        $scope.notifications.push
          message: 'При восстановлении города возникли ошибки'
          status: 'danger'