'use strict'

###*
 # @ngdoc function
 # @name worldCmsApp.controller:ApplicationController
 # @description
 # # ApplicationController
 # Controller of the worldCmsApp
###
angular.module('worldCmsApp')
  .controller 'ApplicationController', ($rootScope, $scope, $location, $http, Auth) ->
    # UI fixes
    $rootScope.showPartials = yes
    $scope.notifications = []

    if window.localStorage.getItem('access_token')?
      $http.defaults.withCredentials = true
      $http.defaults.headers.common.Authorization = window.localStorage.getItem 'access_token'
      Auth.me null
      , (user) ->
        $rootScope.currentUser = user
      , (error) ->
        window.localStorage.removeItem 'access_token'
        $location.path '/login'
    else
      $location.path '/login'

    # Auth protect routes
    $scope.$on '$locationChangeStart', ->
      $location.path '/login' if not window.localStorage.getItem('access_token')?

    $scope.logout = ->
      window.localStorage.removeItem 'access_token'
      $rootScope.currentUser = null
      $location.path '/login'