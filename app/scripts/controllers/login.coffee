'use strict'

###*
 # @ngdoc function
 # @name worldCmsApp.controller:LoginController
 # @description
 # # LoginController
 # Controller of the worldCmsApp
###
angular.module('worldCmsApp')
  .controller 'LoginController', ($rootScope, $scope, Auth, $http, $location) ->
    # UI fix
    $rootScope.showPartials = no
    $scope.$on '$destroy', ->
      $rootScope.showPartials = yes

    $scope.credentials =
      email: null
      password: null

    $scope.login = ->
      Auth.login null, $scope.credentials
      , (user) ->
        if user.id
          $rootScope.currentUser = user
          $http.defaults.headers.common.Authorization = user.access_token
          window.localStorage.setItem 'access_token', user.access_token
          $scope.notifications.push
            status: 'success'
            message: user.first_name + ' ' + user.last_name + ', Вы успешно вошли в панель управления'
          window.location = '#/'
        else
          $scope.notifications.push status: 'danger', message: 'Некорректный email или пароль'
      , (error) ->
        $scope.notifications.push status: 'danger', message: 'Ошибка сервера при попытке входа'