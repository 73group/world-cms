'use strict'

angular.module('worldCmsApp')
  .controller 'RegionsController', ($scope, $filter, Region) ->
    $scope.regions = Region.all()

    $scope.alert =
      message: null
      status: null

    $scope.remove = (region) ->
      $scope.notifications.push
        message: 'Происходит удаление региона'
        status: 'info'
      region.$remove id:region.id
      , (success) ->
        region.deleted_at = $filter('date')(new Date(),'yyyy-MM-dd HH:mm:ss')
        $scope.notifications.push
          message: 'Регион успешно удален'
          status: 'success'
      , (errors) ->
        $scope.notifications.push
          message: errors.data
          status: 'danger'

    $scope.restore = (region) ->
      $scope.notifications.push
        message: 'Происходит восстановление региона'
        status: 'info'
      deletedAt = region.deleted_at
      region.deleted_at = null
      region.$restore id:region.id
      , (success) ->
        $scope.notifications.push
          message: 'Регион успешно восстановлен'
          status: 'success'
      , (error) ->
        region.deleted_at = deletedAt
        $scope.notifications.push
          message: 'При восстановлении региона возникли ошибки'
          status: 'danger'