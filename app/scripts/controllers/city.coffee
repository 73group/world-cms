'use strict'

###*
 # @ngdoc function
 # @name worldCmsApp.controller:CityController
 # @description
 # # CityController
 # Controller of the worldCmsApp
###
angular.module('worldCmsApp')
  .controller 'CityController', ($scope, $routeParams, Region, City) ->
    $scope.regions = Region.all();
    
    $scope.city = if $routeParams.id then City.get id: $routeParams.id else new City
    
    $scope.alert =
      message: null
      status: null

    $scope.save = ->
      if $routeParams.id
        $scope.notifications.push
          message: 'Обновление города'
          status: 'info'
        $scope.city.$update id: $routeParams.id
        , (success) ->
          $scope.notifications.push
            message: 'Город успешно обновлен'
            status: 'success'
        , (error) ->
          $scope.notifications.push
            message: 'При обновлении города возникли ошибки'
            status: 'danger'
      else
        $scope.notifications.push
          message: 'Добавление города'
          status: 'info'
        $scope.city.$save null
        , (success) ->
          $scope.notifications.push
            message: 'Город успешно добавлен'
            status: 'success'
        , (error) ->
          $scope.notifications.push
            message: 'При добавлении города возникли ошибки'
            status: 'danger'