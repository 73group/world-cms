'use strict'

angular.module('worldCmsApp')
  .controller 'ShopsController', ($scope, $filter, Region, City, Site, Shop) ->
    $scope.filter =
      region: null
      city: null
      site: null

    # Get resources and populate filtered fields
    Region.all {}, (regions) ->
      $scope.regions = regions
      
      City.all {}, (cities) ->
        $scope.cities = cities

        Site.all {}, (sites) ->
          sites.forEach (site) ->
            $scope.cities.forEach (city) ->
              site.region_id = city.region_id if city.id is site.city_id
          $scope.sites = sites

        Shop.all {}, (shops) ->
          shops.forEach (shop) ->
            $scope.cities.forEach (city) ->
              shop.region_id = city.region_id if city.id is shop.city_id
          $scope.shops = shops

    # Reset filter
    $scope.$watch 'filter.region', (newRegion)->
      $scope.filter.city = null if not newRegion or $scope.filter.city and newRegion.id isnt $scope.filter.city.region_id
      $scope.filter.site = null if not $scope.filter.city

    $scope.$watch 'filter.city', (newCity)->
      $scope.filter.site = null if not newCity or $scope.filter.site and newCity.id isnt $scope.filter.site.city_id

    # Actions
    $scope.remove = (shop) ->
      $scope.notifications.push
        message: 'Происходит удаление магазина'
        status: 'info'
      shop.$remove id:shop.id
      , (success) ->
        shop.deleted_at = $filter('date')(new Date(),'yyyy-MM-dd HH:mm:ss')
        $scope.notifications.push
          message: 'Магазин успешно удален'
          status: 'success'
      , (error) ->
        $scope.notifications.push
          message: 'При удалении магазина возникли ошибки'
          status: 'danger'

    $scope.restore = (shop) ->
      $scope.notifications.push
        message: 'Происходит восстановление магазина'
        status: 'info'
      deletedAt = shop.deleted_at
      shop.deleted_at = null
      shop.$restore id:shop.id
      , (success) ->
        $scope.notifications.push
          message: 'Магазин успешно восстановлен'
          status: 'success'
      , (error) ->
        shop.deleted_at = deletedAt
        $scope.notifications.push
          message: 'При восстановлении магазина возникли ошибки'
          status: 'danger'