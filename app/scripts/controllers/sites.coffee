'use strict'

angular.module('worldCmsApp')
  .controller 'SitesController', ($scope, $filter, Region, City, Site) ->
    $scope.filter =
      region: null
      city: null

    # Get resources and populate filtered fields
    Region.all {}, (regions) ->
      $scope.regions = regions
      
      City.all {}, (cities) ->
        $scope.cities = cities

        Site.all {}, (sites) ->
          sites.forEach (site) ->
            $scope.cities.forEach (city) ->
              site.region_id = city.region_id if city.id is site.city_id
          $scope.sites = sites

    # Reset filter
    $scope.$watch 'filter.region', (newRegion)->
      $scope.filter.city = null if not newRegion or $scope.filter.city and newRegion.id isnt $scope.filter.city.region_id
      $scope.filter.site = null if not $scope.filter.city

    $scope.remove = (site) ->
      $scope.notifications.push
        message: 'Происходит удаление сайта'
        status: 'info'
      site.$remove id:site.id
      , (success) ->
        site.deleted_at = $filter('date')(new Date(),'yyyy-MM-dd HH:mm:ss')
        $scope.notifications.push
          message: 'Сайт успешно удален'
          status: 'success'
      , (errors) ->
        $scope.notifications.push
          message: errors.data
          status: 'danger'

    $scope.restore = (site) ->
      $scope.notifications.push
        message: 'Происходит восстановление сайта'
        status: 'info'
      deletedAt = site.deleted_at
      site.deleted_at = null
      site.$restore id:site.id
      , (success) ->
        $scope.notifications.push
          message: 'Сайт успешно восстановлен'
          status: 'success'
      , (error) ->
        site.deleted_at = deletedAt
        $scope.notifications.push
          message: 'При восстановлении сайта возникли ошибки'
          status: 'danger'