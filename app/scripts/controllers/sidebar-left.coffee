'use strict'

angular.module('worldCmsApp')
  .controller 'SidebarLeftController', ($scope, $location) ->
    $scope.isActive = (locations) ->
      $location.path() in locations