'use strict'

###*
 # @ngdoc function
 # @name worldCmsApp.controller:SettingsController
 # @description
 # # SettingsController
 # Controller of the worldCmsApp
###
angular.module('worldCmsApp')
  .controller 'SettingsController', ($rootScope, $scope, $location) ->
    $scope.exit = ->
      $rootScope.currentUser.$exit null
      , (success) ->
        window.localStorage.removeItem 'access_token'
        $rootScope.currentUser = null
        $location.path '/login'