'use strict'

###*
 # @ngdoc function
 # @name worldCmsApp.controller:ShopController
 # @description
 # # ShopController
 # Controller of the worldCmsApp
###
angular.module('worldCmsApp')
  .controller 'ShopController', ($scope, $routeParams, City, Site, Shop) ->
    $scope.cities = City.all();
    $scope.sites  = Site.all();
    
    $scope.shop = if $routeParams.id then Shop.get id: $routeParams.id else new Shop
    
    $scope.alert =
      message: null
      status: null

    $scope.save = ->
      if $routeParams.id
        $scope.notifications.push
          message: 'Обновление магазина'
          status: 'info'
        $scope.shop.$update id: $routeParams.id
        , (success) ->
          $scope.notifications.push
            message: 'Магазин успешно обновлен'
            status: 'success'
        , (errors) ->
          $scope.notifications.push
            message: 'При обновлении магазина возникли ошибки'
            status: 'danger'
      else
        $scope.notifications.push
          message: 'Добавление магазина'
          status: 'info'
        $scope.shop.$save null
        , (success) ->
          $scope.notifications.push
            message: 'Магазин успешно добавлен'
            status: 'success'
        , (errors) ->
          $scope.notifications.push
            message: 'При добавлении магазина возникли ошибки'
            status: 'danger'