'use strict'

###*
 # @ngdoc function
 # @name worldCmsApp.controller:ProfileController
 # @description
 # # ProfileController
 # Controller of the worldCmsApp
###
angular.module('worldCmsApp')
  .controller 'ProfileController', ($rootScope, $scope, Auth) ->
    # Profile Data
    $scope.user = Auth.me()
    $scope.save = ->
      $scope.notifications.push
        message: 'Обновление личных данных'
        status: 'info'
      $scope.user.$update null
      , (success) ->
        $rootScope.currentUser = angular.copy $scope.user
        $scope.notifications.push
          message: 'Личные данные успешно обновлены'
          status: 'success'
      , (errors) ->
        angular.forEach errors.data, (errors, field) ->
          $scope.form[field].$setValidity 'server', no
          $scope.form[field].$error.server = errors.join ' '
        $scope.notifications.push
          message: 'При обновлении личных данных возникли ошибки'
          status: 'danger'

    # Change Password
    $scope.passwords =
      old_password: ''
      password: ''
      password_confirmation: ''

    $scope.confirmPassword = ->
      $scope.formPassword.password_confirmation.$invalid = $scope.formPassword.password_confirmation.$error.confirmation = $scope.formPassword.password.$modelValue isnt $scope.formPassword.password_confirmation.$modelValue

    $scope.changePassword = ->
      $scope.notifications.push
        message: 'Изменение пароля'
        status: 'info'
      Auth.changePassword null, $scope.passwords
      , (success) ->
        $scope.notifications.push
          message: 'Ваш пароль успешно изменен'
          status: 'success'
      , (errors) ->
        angular.forEach errors.data, (errors, field) ->
          $scope.formPassword[field].$setValidity 'server', no
          $scope.formPassword[field].$error.server = errors.join ' '
        $scope.notifications.push
          message: 'При изменении пароля возникли ошибки'
          status: 'danger'