'use strict'

###*
 # @ngdoc function
 # @name worldCmsApp.controller:UsersController
 # @description
 # # UsersController
 # Controller of the worldCmsApp
###
angular.module('worldCmsApp')
  .controller 'UsersController', ($scope, $filter, Role, User) ->
    $scope.filter =
      role: null

    $scope.roles = Role.all()
    $scope.users = User.all()

    $scope.remove = (user) ->
      $scope.notifications.push
        message: 'Происходит удаление пользователя'
        status: 'info'
      user.$remove id:user.id
      , (success) ->
        user.deleted_at = $filter('date')(new Date(),'yyyy-MM-dd HH:mm:ss')
        $scope.notifications.push
          message: 'Пользователь успешно удален'
          status: 'success'
      , (errors) ->
        $scope.notifications.push
          message: errors.data
          status: 'danger'

    $scope.restore = (user) ->
      $scope.notifications.push
        message: 'Происходит восстановление пользователя'
        status: 'info'
      deletedAt = user.deleted_at
      user.deleted_at = null
      user.$restore id:user.id
      , (success) ->
        $scope.notifications.push
          message: 'Пользователь успешно восстановлен'
          status: 'success'
      , (error) ->
        user.deleted_at = deletedAt
        $scope.notifications.push
          message: 'При восстановлении пользователя возникли ошибки'
          status: 'danger'