'use strict'

###*
 # @ngdoc function
 # @name worldCmsApp.controller:RegionController
 # @description
 # # RegionController
 # Controller of the worldCmsApp
###
angular.module('worldCmsApp')
  .controller 'RegionController', ($scope, $routeParams, Region) ->
    $scope.region = if $routeParams.id then Region.get id: $routeParams.id else new Region
    
    $scope.alert =
      message: null
      status: null

    $scope.save = ->
      if $routeParams.id
        $scope.notifications.push
          message: 'Обновление региона'
          status: 'info'
        $scope.region.$update id: $routeParams.id
        , (success) ->
          $scope.notifications.push
            message: 'Регион успешно обновлен'
            status: 'success'
        , (error) ->
          $scope.notifications.push
            message: 'При обновлении региона возникли ошибки'
            status: 'danger'
      else
        $scope.notifications.push
          message: 'Добавление региона'
          status: 'info'
        $scope.region.$save null
        , (success) ->
          $scope.notifications.push
            message: 'Регион успешно добавлен'
            status: 'success'
        , (error) ->
          $scope.notifications.push
            message: 'При добавлении региона возникли ошибки'
            status: 'danger'