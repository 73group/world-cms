'use strict'

###*
 # @ngdoc function
 # @name worldCmsApp.controller:SiteController
 # @description
 # # SiteController
 # Controller of the worldCmsApp
###
angular.module('worldCmsApp')
  .controller 'SiteController', ($scope, $routeParams, City, Site) ->
    $scope.cities = City.all();
    
    $scope.site = if $routeParams.id then Site.get id: $routeParams.id else new Site
    
    $scope.alert =
      message: null
      status: null

    $scope.save = ->
      if $routeParams.id
        $scope.notifications.push
          message: 'Обновление сайта'
          status: 'info'
        $scope.site.$update id: $routeParams.id
        , (success) ->
          $scope.notifications.push
            message: 'Сайт успешно обновлен'
            status: 'success'
        , (errors) ->
          angular.forEach errors.data, (errors, field) ->
            $scope.form[field].$setValidity 'server', no
            $scope.form[field].$error.server = errors.join ' '
          $scope.notifications.push
            message: 'При обновлении сайта возникли ошибки'
            status: 'danger'
      else
        $scope.notifications.push
          message: 'Добавление сайта'
          status: 'info'
        $scope.site.$save null
        , (success) ->
          $scope.notifications.push
            message: 'Сайт успешно добавлен'
            status: 'success'
        , (errors) ->
          angular.forEach errors.data, (errors, field) ->
            $scope.form[field].$setValidity 'server', no
            $scope.form[field].$error.server = errors.join ' '
          $scope.notifications.push
            message: 'При добавлении сайта возникли ошибки'
            status: 'danger'