'use strict'

###*
 # @ngdoc directive
 # @name worldCmsApp.directive:serverError
 # @description
 # # serverError
###
angular.module('worldCmsApp')
  .directive('serverError', ->
    restrict: 'A'
    require: '?ngModel'
    link: (scope, element, attrs, ctrl) ->
      element.on 'keyup', ->
        scope.$apply ->
          ctrl.$setValidity 'server', true
  )
