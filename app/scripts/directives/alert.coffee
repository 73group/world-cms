'use strict'

###*
 # @ngdoc directive
 # @name worldCmsApp.directive:alert
 # @description
 # # alert
###
angular.module('worldCmsApp')
  .directive('alert', ->
    templateUrl: 'views/partials/alert.html'
    restrict: 'E'
  )
