'use strict'

###*
 # @ngdoc directive
 # @name worldCmsApp.directive:notifications
 # @description
 # # notifications
###
angular.module('worldCmsApp')
  .directive('notifications', ($timeout) ->
    templateUrl: 'views/partials/notifications.html'
    restrict: 'E'
    link: ($scope) ->
      $scope.$watchCollection 'notifications', ->
        $timeout ->
          $scope.notifications.splice 0, 1
        , 10000
  )
