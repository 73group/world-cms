'use strict'

angular
  .module('worldCmsApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute'
  ])
  .constant('apiBase', '//api.frames.local/')
  .constant('apiRoot', '//api.frames.local/root/')
  .config ($routeProvider) ->
    $routeProvider
      # Home
      .when '/',
        controller:  'MainController',
        templateUrl: 'views/main.html'
      # Users
      .when '/login',
        controller:  'LoginController',
        templateUrl: 'views/users/login.html'
      .when '/profile',
        controller:  'ProfileController',
        templateUrl: 'views/users/profile.html'
      .when '/settings',
        controller:  'SettingsController',
        templateUrl: 'views/users/settings.html'
      .when '/users',
        controller:  'UsersController',
        templateUrl: 'views/users/list.html'
      # Regions
      .when '/regions',
        controller:  'RegionsController',
        templateUrl: 'views/regions/list.html'
      .when '/regions/create',
        controller:  'RegionController',
        templateUrl: 'views/regions/form.html'
      .when '/regions/:id/edit',
        controller:  'RegionController',
        templateUrl: 'views/regions/form.html'
      # Cities
      .when '/cities',
        controller:  'CitiesController',
        templateUrl: 'views/cities/list.html'
      .when '/cities/create',
        controller:  'CityController',
        templateUrl: 'views/cities/form.html'
      .when '/cities/:id/edit',
        controller:  'CityController',
        templateUrl: 'views/cities/form.html'
      # Sites
      .when '/sites',
        controller:  'SitesController',
        templateUrl: 'views/sites/list.html'
      .when '/sites/create',
        controller:  'SiteController',
        templateUrl: 'views/sites/form.html'
      .when '/sites/:id/edit',
        controller:  'SiteController',
        templateUrl: 'views/sites/form.html'
      # Shops
      .when '/shops',
        controller:  'ShopsController',
        templateUrl: 'views/shops/list.html'
      .when '/shops/create',
        controller:  'ShopController',
        templateUrl: 'views/shops/form.html'
      .when '/shops/:id/edit',
        controller:  'ShopController',
        templateUrl: 'views/shops/form.html'
      # Default
      .otherwise
        redirectTo: '/'
  .run ($http) ->
    $http.defaults.withCredentials = true