'use strict'

###*
 # @ngdoc filter
 # @name worldCmsApp.filter:fromNow
 # @function
 # @description
 # # fromNow
 # Filter in the worldCmsApp.
###
angular.module('worldCmsApp')
  .filter 'fromNow', ->
    (datetime) ->
      moment.utc(datetime).fromNow()
