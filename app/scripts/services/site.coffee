'use strict'

###*
 # @ngdoc service
 # @name worldCmsApp.Site
 # @description
 # # Site
 # Service in the worldCmsApp.
###
angular.module('worldCmsApp')
  .service 'Site', ($resource, apiRoot) ->
    resource = 'sites'
    urls =
      collection: apiRoot + resource
      item: apiRoot + resource + '/:id'

    $resource urls.item, null,
      all:
        url: urls.collection
        method: 'GET'
        isArray: yes
      
      update:
        method: 'PUT'

      restore:
        url: urls.item + '/restore'
        method: 'PATCH'