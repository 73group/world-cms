'use strict'

###*
 # @ngdoc service
 # @name worldCmsApp.City
 # @description
 # # City
 # Service in the worldCmsApp.
###
angular.module('worldCmsApp')
  .service 'City', ($resource, apiRoot) ->
    resource = 'cities'
    urls =
      collection: apiRoot + resource
      item: apiRoot + resource + '/:id'

    $resource urls.item, null,
      all:
        url: urls.collection
        method: 'GET'
        isArray: yes
      
      update:
        method: 'PUT'

      restore:
        url: urls.item + '/restore'
        method: 'PATCH'