'use strict'

###*
 # @ngdoc service
 # @name worldCmsApp.Region
 # @description
 # # Region
 # Service in the worldCmsApp.
###
angular.module('worldCmsApp')
  .service 'Region', ($resource, apiRoot) ->
    resource = 'regions'
    urls =
      collection: apiRoot + resource
      item: apiRoot + resource + '/:id'

    $resource urls.item, null,
      all:
        url: urls.collection
        method: 'GET'
        isArray: yes
      
      update:
        method: 'PUT'

      restore:
        url: urls.item + '/restore'
        method: 'PATCH'