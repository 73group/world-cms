'use strict'

###*
 # @ngdoc service
 # @name worldCmsApp.Auth
 # @description
 # # Auth
 # Service in the worldCmsApp.
###
angular.module('worldCmsApp')
  .service 'Auth', ($resource, apiBase) ->
    $resource apiBase + 'users/me', null,
      login:
        url: apiBase + 'login'
        method: 'POST'

      me:
        method: 'GET'

      update:
        method: 'PUT'

      exit:
        url: apiBase + 'users/me/tokens'
        method: 'DELETE'

      changePassword:
        url: apiBase + 'users/me/password'
        method: 'PUT'