'use strict'

angular.module('worldCmsApp')
  .service 'Shop', ($resource, apiRoot) ->
    resource = 'shops'
    urls =
      collection: apiRoot + resource
      item: apiRoot + resource + '/:id'

    $resource urls.item, null,
      all:
        url: urls.collection
        method: 'GET'
        isArray: yes
      
      update:
        method: 'PUT'

      restore:
        url: urls.item + '/restore'
        method: 'PATCH'