'use strict'

###*
 # @ngdoc service
 # @name worldCmsApp.Role
 # @description
 # # Role
 # Service in the worldCmsApp.
###
angular.module('worldCmsApp')
  .service 'Role', ($resource, apiRoot) ->
    resource = 'roles'
    urls =
      collection: apiRoot + resource
      item: apiRoot + resource + '/:id'

    $resource urls.item, null,
      all:
        url: urls.collection
        method: 'GET'
        isArray: yes