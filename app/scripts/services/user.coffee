'use strict'

###*
 # @ngdoc service
 # @name worldCmsApp.User
 # @description
 # # User
 # Service in the worldCmsApp.
###
angular.module('worldCmsApp')
  .service 'User', ($resource, apiRoot) ->
    resource = 'users'
    urls =
      collection: apiRoot + resource
      item: apiRoot + resource + '/:id'

    $resource urls.item, null,
      all:
        url: urls.collection
        method: 'GET'
        isArray: yes

      restore:
        url: urls.item + '/restore'
        method: 'PATCH'